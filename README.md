# GetItOnCodeberg

Get It On Codeberg Badge

![Get it on Codeberg](get-it-on-blue-on-white.png)

![Get it on Codeberg Legacy](get-it-on-legacy.png)

Example:

```
<a href="__add_your_repository_url_here__">
    <img alt="Get it on Codeberg" src="https://pages.codeberg.org/pstorch/get-it-on-legacy.png" height="60">
</a>
```

Generate your own Badge online: https://get-it-on.codeberg.org/

